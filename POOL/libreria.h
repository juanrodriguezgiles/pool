#ifndef LIBRERIA_H
#define LIBRERIA_H
#include "raylib.h"
#include <cmath>
bool hayColision2Circ(Vector2 c1, float r1, Vector2 c2, float r2);
bool hayColisionCircRec(Vector2 c1, float r1, Rectangle rec);
#endif // !LIBRERIA_H

