#include "classPelota.h"
namespace tipografia
{
	int chica = 5;
}
pelota::pelota(Vector2 pos, int n)
{
	_pos = pos;		//posicion.
	_posInit = pos; //posicion inicial.
	_radio = 10;	//radio.
	_vel = { 0,0 };
	_PDeImpc = { 0,0 };
	_acel = { 0,0 };
	for (int i = 0; i < cantidadPelotas; i++)
	{
		_col[i] = false;
	}
	_activo = true;
	txload(n);
}
//-----------Draws------------------------
void pelota::drawPelota()
{
	DrawTexture(tx, _pos.x - _radio / 2, _pos.y - _radio / 2, WHITE);
}
void pelota::updatePDeImpc(Vector2 x)
{
	if (!(_vel.x != 0) && !(_vel.y != 0))
	{
		if (!IsMouseButtonDown(MOUSE_LEFT_BUTTON))
		{
			Vector2 mouse = x;
			//obtengo un punto de la circundeferencia del circulo mas proximo a el mouse.
			_PDeImpc.x = _pos.x - 1 * (_radio * (_pos.x - mouse.x)) / sqrt(powf(static_cast<float>(_pos.x - mouse.x), 2) + powf(static_cast<float>(_pos.y - mouse.y), 2));
			_PDeImpc.y = _pos.y - 1 * (_radio * (_pos.y - mouse.y)) / sqrt(powf(static_cast<float>(_pos.x - mouse.x), 2) + powf(static_cast<float>(_pos.y - mouse.y), 2));
		}
	}
}
void pelota::drawPDeImpc()
{
	DrawCircle(_PDeImpc.x, _PDeImpc.y, 2, RED);
}
void pelota::drawPelota(short i)
{
	DrawText(FormatText("%i", i), _pos.x - MeasureText("x", tipografia::chica), _pos.y, tipografia::chica, RED);
	DrawTexture(tx, _pos.x - _radio, _pos.y - _radio, WHITE);
}
//----------------------------------------
//--------------input---------------------
void pelota::reiniciar()
{
	_pos = _posInit;
	_vel = { 0,0 };
}
void pelota::datos()
{
	if (IsKeyPressed(KEY_H))
	{
		std::cout << "Velocidad = " << _vel.x << "," << _vel.y << std::endl;
	}
}
void pelota::hacks()
{
	move();
	reiniciar();
	datos();
}
void pelota::move()
{
	if (IsKeyDown(KEY_UP)) { _pos.y -= 3; }
	if (IsKeyDown(KEY_DOWN)) { _pos.y += 3; }
	if (IsKeyDown(KEY_LEFT)) { _pos.x -= 3; }
	if (IsKeyDown(KEY_RIGHT)) { _pos.x += 3; }

	if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON))
	{
		_pos = GetMousePosition();
	}
}
//-----------------------------------------
//--------------Logic----------------------
void pelota::golpearPared(Rectangle bordesLargos[4], Rectangle bordesLaterales[2])
{
	for (int i = 0; i < 4; i++)
	{
		if (hayColisionCircRec(_pos, _radio, bordesLargos[i]))
		{
			_vel.y *= -1;

		}
	}
	for (int i = 0; i < 2; i++)
	{
		if (hayColisionCircRec(_pos, _radio, bordesLaterales[i]))
		{
			_vel.x *= -1;
		}
	}
}
bool pelota::entroAgujero(Vector2 agujeros[6])
{
	for (int i = 0; i < 6; i++)
	{
		if (hayColision2Circ(_pos, _radio, agujeros[i], 30))
		{
			_activo = false;
			_pos.x = 0;
			_pos.y = GetScreenHeight() * 2;
			return true;
		}
	}
	return false;
}
void pelota::actualizar()
{
	// Update Ball Positions
	// Add Drag to emulate rolling friction
	_acel.x = -_vel.x * ResistenciaDeLaMesa;
	_acel.y = -_vel.y * ResistenciaDeLaMesa;

	// Update ball physics
	_vel.x += _acel.x * GetFrameTime();
	_vel.y += _acel.y * GetFrameTime();
	_pos.x += _vel.x * GetFrameTime() * GetScreenWidth() / 10;
	_pos.y += _vel.y * GetFrameTime() * GetScreenHeight() / 10;

	// Stop balls if _vel = FrenaCuandoVesmenorA
	//H*2 = C2 + C2;
	if ((_vel.x * _vel.x + _vel.y * _vel.y) < FrenaCuandoVesmenorA)
	{
		_vel.x = 0;
		_vel.y = 0;
	}
}
void pelota::drawfuerza(Vector2& fuerza) {
	DrawRectangle(0, GetScreenHeight() - 10, fabs(fuerza.x * 5) + fabs(fuerza.y * 5), 10, RED);
}
void pelota::pegarle(Vector2& fuerza)
{
	Vector2 mouse = GetMousePosition();
	if (_vel.x == 0 && _vel.y == 0)
	{
		if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
		{
			if (IsMouseButtonDown(MOUSE_RIGHT_BUTTON))
			{
				//para cancelar el tiro puedo apretar segundo boton.
				fuerza.x = 0;
				fuerza.y = 0;
			}
			else
			{
				//calculo el angulo que se forma con el centro de la pelota a el mouse.
				int opuesto = _pos.y - mouse.y;
				int adyacente = _pos.x - mouse.x;
				float AnguloResultante = atan2(opuesto, adyacente);
				AnguloResultante = AnguloResultante * 180 / 3.14f; //lo paso a grados celcius.
				if (IsKeyPressed(KEY_A))
				{
					std::cout << "Este es el angulo:" << AnguloResultante;
				}
				/*
								90�
								 |
							  00000000
							 0000000000
						   00000000000000
					0�---- 00000000000000  ----  180� o -180� si voy desde abajo
						   00000000000000
							 0000000000
							  00000000
								 |
								-90�


				*/
				int golpe_maximo = 20;
				if (fabs(fuerza.x) + fabs(fuerza.y) <= golpe_maximo)
				{
					//para darle medida a la fuerza, tomo la posicion de la pelota y el punto de impacto y sumos sus x e y
					//a una proporcion muy baja, por eso se divide en velocidad de carga.
					int velocidad_de_carga = 200;
					fuerza.x += (_PDeImpc.x - _pos.x) / velocidad_de_carga;
					fuerza.y += (_PDeImpc.y - _pos.y) / velocidad_de_carga;
				}
				else
				{
					//se loquea la fuerza una vez pasada la distancia del golpe maximo.
				}
			}
		}
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			_vel = { -fuerza.x,-fuerza.y }; //se lo paso a la velocidad de la pelota.
			fuerza.x = 0;
			fuerza.y = 0;
		}
	}
}
//-----------------------------------------
//--------------Set/get--------------------
void pelota::setCol(int ball, bool a)
{
	_col[ball] = a;
}
bool pelota::getCol(int ball)
{
	return _col[ball];
}
bool pelota::getActivo()
{
	return _activo;
}
void pelota::setActivo(bool a)
{
	_activo = a;
}
void pelota::txload(int n)
{
	switch (n)
	{
	case 0:
		tx = LoadTexture("assets/ball_16.png");
		break;
	case 1:
		tx = LoadTexture("assets/ball_13.png");
		break;
	case 2:
		tx = LoadTexture("assets/ball_14.png");
		break;
	case 3:
		tx = LoadTexture("assets/ball_15.png");
		break;
	case 4:
		tx = LoadTexture("assets/ball_12.png");
		break;
	case 5:
		tx = LoadTexture("assets/ball_11.png");
		break;
	case 6:
		tx = LoadTexture("assets/ball_10.png");
		break;
	case 7:
		tx = LoadTexture("assets/ball_9.png");
		break;
	case 8:
		tx = LoadTexture("assets/ball_7.png");
		break;
	case 9:
		tx = LoadTexture("assets/ball_8.png");
		break;
	case 10:
		tx = LoadTexture("assets/ball_5.png");
		break;
	case 11:
		tx = LoadTexture("assets/ball_6.png");
		break;
	case 12:
		tx = LoadTexture("assets/ball 4.png");
		break;
	case 13:
		tx = LoadTexture("assets/ball 3.png");
		break;
	case 14:
		tx = LoadTexture("assets/ball 2.png");
		break;
	case 15:
		tx = LoadTexture("assets/ball 1.png");
		break;
	default:
		//tx = LoadTexture("assets/ball_16.png");
		break;
	}
	tx.width = _radio*2;
	tx.height = _radio*2;
}
void pelota::setPosition(Vector2 pos)
{
	_pos = pos;
	_posInit = pos;
}
void pelota::setVel(Vector2 vel)
{
	_vel = vel;
}
Vector2 pelota::getPos()
{
	return _pos;
}
int pelota::getRadio()
{
	return _radio;
}
Vector2 pelota::getVel()
{
	return _vel;
}

//-----------Taco--------------------------
taco::taco()
{
	_fuerza = { 0,0 };
}
Vector2 taco::getterFuerza()
{
	return _fuerza;
}
