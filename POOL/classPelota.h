#ifndef CLASS_PELOTA
#define CLASS_PELOTA
#include "raylib.h"
#include "libreria.h"
#include <math.h>
#include <iostream>

const float ResistenciaDeLaMesa = 0.2f;//se podria entender como gravedad, mientras mas alto, mas rapido frena, mas rosamiento.
									   //valores de 2 a 0.1 pero va a tardar mil a�os en frenar en 0.1f.
									   //la bola 0 va a ser la blanca.
const float FrenaCuandoVesmenorA = 0.2f;
const int cantidadPelotas = 16;

namespace tipografia
{
	extern int chica;
}
class pelota
{
public:
	//creador.
	pelota(Vector2 pos, int n);
	//dibujado.
	void drawPelota();
	void drawPelota(short i);
	//control.
	void reiniciar();
	void golpearPared(Rectangle bordesLargos[4], Rectangle bordesLaterales[2]);
	//modif
	bool entroAgujero(Vector2 agujeros[6]);
	void updatePDeImpc(Vector2 x);
	void drawPDeImpc();
	void datos();
	void hacks();
	void pegarle(Vector2& fuerza);
	void actualizar();
	void drawfuerza(Vector2& fuerza);
	void setPosition(Vector2 pos);
	Vector2 getPos();
	int getRadio();
	Vector2 getVel();
	void setVel(Vector2 vel);
	void move();
	void setCol(int ball, bool a);
	bool getCol(int ball);
	//add
	bool getActivo();
	void setActivo(bool a);
	void txload(int n);
private:
	Vector2 _posInit;
	int	      _radio;
	Vector2    _acel;
	Vector2     _pos;
	Vector2     _vel;
	Vector2	_PDeImpc;
	bool	_col[cantidadPelotas];
	bool _activo;
	Texture2D tx;
};
class taco
{
public:
	taco();
	Vector2 getterFuerza();
	Vector2 _fuerza;

private:
};
#endif
