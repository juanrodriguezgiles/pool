#include "libreria.h"
bool hayColision2Circ(Vector2 c1, float r1, Vector2 c2, float r2)
{
	int distancia = sqrt((c1.x-c2.x)* (c1.x - c2.x)+(c1.y-c2.y)* (c1.y - c2.y));

	return (distancia < r1 + r2);
}
bool hayColisionCircRec(Vector2 c1, float r1, Rectangle rec)
{
	float pX = c1.x;
	float pY = c1.y;
	float distancia;

	if (pX < rec.x)
		pX = rec.x;
	if (pX > rec.x + rec.width)
		pX = rec.x + rec.width;
	if (pY < rec.y)
		pY = rec.y;
	if (pY > rec.y + rec.height)
		pY = rec.y + rec.height;

	distancia = sqrt((c1.x - pX) * (c1.x - pX) + (c1.y - pY) * (c1.y - pY));

	return distancia < r1;
}