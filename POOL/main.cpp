﻿#include "raylib.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "classPelota.h"
#include "libreria.h"
#define π 3.14
//----------------------------------------------------------------------------------
void PosicionarPelotas(pelota* balls[cantidadPelotas]);
void collitionChekPelotas(pelota* balls[cantidadPelotas]);
bool win(pelota* balls[cantidadPelotas]);
//----------------------------------------------------------------------------------
// Types and Structures Definition
//----------------------------------------------------------------------------------
pelota* pBlanca;
taco* tacoJugador;
pelota* pelotas[cantidadPelotas];
Rectangle linea;
Rectangle linea2;
Rectangle bordesLargos[4];
Rectangle bordesLaterales[2];
Vector2 agujeros[6];
int radioAgujero = 30;
Texture2D fondo;
//------------------------------------------------------------------------------------
// Global Variables Declaration
//------------------------------------------------------------------------------------
static const int screenWidth = 800;
static const int screenHeight = 450;
static bool gameOver = false;
static bool pause = false;
bool win1 = false;
bool loose = false;
int lifes = 3;
//------------------------------------------------------------------------------------
// Module Functions Declaration (local)
//------------------------------------------------------------------------------------
static void InitGame(void);         // Initialize game
static void UpdateGame(void);       // Update game (one frame)
static void DrawGame(void);         // Draw game (one frame)
static void UnloadGame(void);       // Unload game
static void UpdateDrawFrame(void);  // Update and Draw (one frame)
static void generarBordes()
{
	bordesLaterales[0].x = 0;
	bordesLaterales[0].y = 50 + radioAgujero;
	bordesLaterales[0].width = 50;
	bordesLaterales[0].height = screenHeight - radioAgujero * 2 - 100;
	bordesLaterales[1].x = screenWidth - 50;
	bordesLaterales[1].y = 50 + radioAgujero;
	bordesLaterales[1].width = 50;
	bordesLaterales[1].height = screenHeight - radioAgujero * 2 - 100;

	bordesLargos[0].x = radioAgujero + 40;
	bordesLargos[0].y = 0;
	bordesLargos[0].width = screenWidth / 2 - radioAgujero * 2 - 40;
	bordesLargos[0].height = 50;
	bordesLargos[1].x = radioAgujero * 3 + 40 + screenWidth / 2 - radioAgujero * 2 - 50;
	bordesLargos[1].y = 0;
	bordesLargos[1].width = screenWidth / 2 - radioAgujero * 2 - 40;
	bordesLargos[1].height = 50;

	bordesLargos[2].x = bordesLargos[0].x;
	bordesLargos[2].y = screenHeight - bordesLargos[0].height;
	bordesLargos[2].width = bordesLargos[0].width;
	bordesLargos[2].height = bordesLargos[0].height;
	bordesLargos[3].x = bordesLargos[1].x;
	bordesLargos[3].y = screenHeight - bordesLargos[1].height;
	bordesLargos[3].width = bordesLargos[1].width;
	bordesLargos[3].height = bordesLargos[1].height;

	agujeros[0].x = 30;
	agujeros[0].y = 30;

	agujeros[1].x = screenWidth / 2;
	agujeros[1].y = 20;

	agujeros[2].x = screenWidth - 30;
	agujeros[2].y = 30;

	agujeros[3].x = 30;
	agujeros[3].y = screenHeight - 30;

	agujeros[4].x = screenWidth / 2;
	agujeros[4].y = screenHeight - 20;

	agujeros[5].x = screenWidth - 30;
	agujeros[5].y = screenHeight - 30;
}
//------------------------------------------------------------------------------------
// Program main entry point
//------------------------------------------------------------------------------------
int main(void)
{
	// Initialization (Note windowTitle is unused on Android)
	//---------------------------------------------------------
	InitWindow(screenWidth, screenHeight, "POOL");

	InitGame();

	SetTargetFPS(60);
	//--------------------------------------------------------------------------------------

	// Main game loop
	while (!WindowShouldClose())    // Detect window close button or ESC key
	{
		// Update and Draw
		//----------------------------------------------------------------------------------
		UpdateDrawFrame();
		//----------------------------------------------------------------------------------
	}

	// De-Initialization
	//--------------------------------------------------------------------------------------
	UnloadGame();         // Unload loaded data (textures, sounds, models...)

	CloseWindow();        // Close window and OpenGL context
	//--------------------------------------------------------------------------------------

	return 0;
}
//------------------------------------------------------------------------------------
// Module Functions Definitions (local)
//------------------------------------------------------------------------------------
void InitGame(void)
{
	fondo = LoadTexture("assets/table.png");
	fondo.height = GetScreenHeight();
	fondo.width = GetScreenWidth();
	tacoJugador = new taco();
	for (int i = 0; i < cantidadPelotas; i++)
	{
		pelotas[i] = new pelota({ 0,0 }, i);
	}
	PosicionarPelotas(pelotas);
	generarBordes();
}
void UpdateGame(void)
{
	if (!gameOver)
	{
		if (!pause)
		{
			if (IsKeyPressed('P')) pause = !pause;
			pelotas[0]->pegarle(tacoJugador->_fuerza);
			pelotas[0]->datos();
			pelotas[0]->updatePDeImpc(GetMousePosition());
			pelotas[0]->move();
			for (int i = 0; i < cantidadPelotas; i++)
			{
				pelotas[i]->actualizar();
				if (pelotas[i]->entroAgujero(agujeros))
				{
					win1 = win(pelotas);
					if (win1 == true)
					{
						gameOver = true;
					}
					if (i == 0)
					{
						lifes--;
						if (lifes < 0)
						{
							gameOver = true;
							loose = true;
						}
						pelotas[0]->reiniciar();
					}
					if (i == 9)
					{
						gameOver = true;
						loose = true;
					}
				}
				pelotas[i]->golpearPared(bordesLargos, bordesLaterales);
				if (IsKeyPressed(KEY_R))
				{
					pelotas[i]->reiniciar();
				}
			}
			collitionChekPelotas(pelotas);
		}
		else
			if (IsKeyPressed('P')) pause = !pause;
	}
	else if (IsKeyPressed(KEY_R))
	{
		for (int i = 0; i < cantidadPelotas; i++)
		{
			pelotas[i]->reiniciar();
			pelotas[i]->setActivo(true);
		}
		gameOver = false;
		win1 = false;
		loose = false;
	}
}
void DrawGame(void)
{
	BeginDrawing();
	ClearBackground(BLACK);

	DrawTexture(fondo, 0, 0, WHITE);
	for (int i = 0; i < cantidadPelotas; i++)
	{
		pelotas[i]->drawPelota(i);
		if (CheckCollisionPointCircle(GetMousePosition(), pelotas[i]->getPos(), pelotas[i]->getRadio()))
			std::cout << "Soy la p " << i << "esta pelota esta en " << pelotas[i]->getPos().x << "," << pelotas[i]->getPos().y;
	}
	DrawLine(linea.x, linea.y, linea.width, linea.height, RED);
	DrawLine(linea2.x, linea2.y, linea2.width, linea2.height, RED);
	pelotas[0]->drawPDeImpc();
	pelotas[0]->drawfuerza(tacoJugador->_fuerza);
	if (pause)
	{
		DrawText("PAUSE", GetScreenWidth() / 2 - (MeasureText("PAUSE", GetScreenHeight() / 10)) / 2, GetScreenHeight() / 2 - GetScreenHeight() / 10 / 2, GetScreenHeight() / 10, RED);
	}
	if (gameOver)
	{
		if (win1)
		{
			DrawText("YOU WIN", GetScreenWidth() / 2 - (MeasureText("YOU WIN", GetScreenHeight() / 10)) / 2, GetScreenHeight() / 2 - GetScreenHeight() / 10 / 2, GetScreenHeight() / 10, RED);
		}
		if (loose)
		{
			DrawText("YOU LOSE", GetScreenWidth() / 2 - (MeasureText("YOU LOSE", GetScreenHeight() / 10)) / 2, GetScreenHeight() / 2 - GetScreenHeight() / 10 / 2, GetScreenHeight() / 10, RED);
		}
	}

	EndDrawing();
}
void UnloadGame(void)
{
	CloseWindow();
}
void UpdateDrawFrame(void)
{
	UpdateGame();
	DrawGame();
}
void espejar(pelota* balls[cantidadPelotas])
{
	for (int i = 0; i < cantidadPelotas; i++)
	{
		balls[i]->setPosition({ balls[i]->getPos().x + static_cast<float>(GetScreenWidth() / 2),balls[i]->getPos().y });
	}
}
void PosicionarPelotas(pelota* balls[cantidadPelotas])
{
	float screenHeight = static_cast<float>(GetScreenHeight());
	Vector2 aux = { GetScreenWidth() / 10 * 7, screenHeight / 2 };
	balls[0]->setPosition(aux);
	Rectangle POOL_TABLE = { 0,0,GetScreenWidth(),GetScreenHeight() };
	float POOL_TABLE_FRAME_SIZE = 3;
	float SQRT_THREE = 1.73205f;
	float posicionX = POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * 0.125f;
	aux = { posicionX, screenHeight / 2 };
	balls[1]->setPosition(aux);
	balls[0]->setPosition({ posicionX - 200, screenHeight / 2 });
	balls[1]->setPosition({ posicionX, screenHeight / 2 });
	aux = { posicionX, (screenHeight / 2) + (balls[1]->getRadio() * 2) + 2 };
	balls[2]->setPosition(aux);
	aux = { posicionX, (screenHeight / 2) + (balls[1]->getRadio() * 4) + 4 };
	balls[3]->setPosition(aux);
	aux = { posicionX, (screenHeight / 2) - balls[1]->getRadio() * 2 - 2 };
	balls[4]->setPosition(aux);
	aux = { posicionX, (screenHeight / 2) - balls[1]->getRadio() * 4 - 4 };
	balls[5]->setPosition(aux);
	aux = { posicionX - SQRT_THREE * balls[1]->getRadio() - 5, (screenHeight / 2) + balls[1]->getRadio() * 3 + 4 };
	balls[6]->setPosition(aux);
	aux = { posicionX - SQRT_THREE * balls[1]->getRadio() - 5, (screenHeight / 2) + balls[1]->getRadio() + 2 };
	balls[7]->setPosition(aux);
	aux = { posicionX - SQRT_THREE * balls[1]->getRadio() - 5, (screenHeight / 2) - balls[1]->getRadio() * 3 - 4 };
	balls[8]->setPosition(aux);
	aux = { posicionX - SQRT_THREE * balls[1]->getRadio() - 5, (screenHeight / 2) - balls[1]->getRadio() - 2 };
	balls[9]->setPosition(aux);
	aux = { posicionX - SQRT_THREE * balls[1]->getRadio() * 2 - 7, screenHeight / 2 };
	balls[10]->setPosition(aux);
	aux = { posicionX - SQRT_THREE * balls[1]->getRadio() * 2 - 7, (screenHeight / 2) + balls[1]->getRadio() * 2 + 2 };
	balls[11]->setPosition(aux);
	aux = { posicionX - SQRT_THREE * balls[1]->getRadio() * 2 - 7, (screenHeight / 2) - balls[1]->getRadio() * 2 - 2 };
	balls[12]->setPosition(aux);
	aux = { posicionX - SQRT_THREE * balls[1]->getRadio() * 3 - 10, (screenHeight / 2) + balls[1]->getRadio() + 2 };
	balls[13]->setPosition(aux);
	aux = { posicionX - SQRT_THREE * balls[1]->getRadio() * 3 - 10, (screenHeight / 2) - balls[1]->getRadio() - 2 };
	balls[14]->setPosition(aux);
	aux = { posicionX - SQRT_THREE * balls[1]->getRadio() * 4 - 10, screenHeight / 2 };
	balls[15]->setPosition(aux);
	espejar(balls);
}
void colisionaron(pelota* balls1, pelota* balls2)
{
	float fDistancia = sqrtf((balls1->getPos().x - balls2->getPos().x) * (balls1->getPos().x - balls2->getPos().x) + (balls1->getPos().y - balls2->getPos().y) * (balls1->getPos().y - balls2->getPos().y));

	//normal
	float nx = ((balls2->getPos().x - balls1->getPos().x) / fDistancia);
	float ny = ((balls2->getPos().y - balls1->getPos().y) / fDistancia);

	//Tangente
	float tx = -ny;
	float ty = nx;

	//producto punto Tangente
	float dpTan1 = balls1->getVel().x * tx + balls1->getVel().y * ty;
	float dpTan2 = balls2->getVel().x * tx + balls2->getVel().y * ty;

	// producto punto Normal
	float dpNorm1 = balls1->getVel().x * nx + balls1->getVel().y * ny;
	float dpNorm2 = balls2->getVel().x * nx + balls2->getVel().y * ny;
	float mass = 1;

	// conservacion de momento en una dimension
	float m1 = (dpNorm1 * 0 + 2.0f * mass * dpNorm2) / (mass + mass);
	float m2 = (dpNorm2 * 0 + 2.0f * mass * dpNorm1) / (mass + mass);

	//como las masas son iguales, da 0.
	balls1->setVel(Vector2{ tx * dpTan1 + nx * m1 ,ty * dpTan1 + ny * m1 });
	balls2->setVel(Vector2{ tx * dpTan2 + nx * m2 , ty * dpTan2 + ny * m2 });
}
void repulsion(pelota* balls1, pelota* balls2)
{
	if (balls1->getPos().x < balls2->getPos().x)
	{
		if (balls1->getPos().y < balls2->getPos().y)
		{
			balls2->setVel({ balls2->getVel().x + 0.1f,balls2->getVel().y + 0.1f });
			balls1->setVel({ balls1->getVel().x - 0.1f,balls1->getVel().y - 0.1f });
		}
		else
		{
			balls2->setVel({ balls2->getVel().x + 0.1f,balls2->getVel().y - 0.1f });
			balls1->setVel({ balls1->getVel().x - 0.1f,balls1->getVel().y + 0.1f });
		}
	}
	else
	{
		if (balls1->getPos().y < balls2->getPos().y)
		{
			balls2->setVel({ balls2->getVel().x - 0.1f,balls2->getVel().y + 0.1f });
			balls1->setVel({ balls1->getVel().x + 0.1f,balls1->getVel().y - 0.1f });
		}
		else
		{
			balls2->setVel({ balls2->getVel().x - 0.1f,balls2->getVel().y - 0.1f });
			balls1->setVel({ balls1->getVel().x + 0.1f,balls1->getVel().y + 0.1f });
		}
	}
}
void collitionChekPelotas(pelota* balls[cantidadPelotas])
{
	for (int i = 0; i < cantidadPelotas; i++)
	{
		for (int w = i + 1; w < cantidadPelotas; w++)
		{
			if (i != w)
			{
				if (hayColision2Circ(balls[i]->getPos(), balls[i]->getRadio(), balls[w]->getPos(), balls[w]->getRadio())) //si colicionan.
				{
					if (!balls[i]->getCol(w))
					{
						colisionaron(balls[i], balls[w]);
						balls[i]->setCol(w, true);
						balls[w]->setCol(i, true);
					}
					else
					{
						repulsion(balls[i], balls[w]);
					}
				}
				else
				{
					balls[i]->setCol(w, false);
					balls[w]->setCol(i, false);
				}
			}
		}
	}
}
bool win(pelota* balls[cantidadPelotas])
{
	for (int i = 1; i < cantidadPelotas; i++)
	{
		if (balls[i]->getActivo())
			return false;
	}
	return true;
}